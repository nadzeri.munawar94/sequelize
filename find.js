const { Article, User } = require('./models');

Article.findAll({ include: User })
    .then((articles) => console.log(articles));