'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.addColumn('Users', 'external_id', { 
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
    });

    await queryInterface.addColumn('Articles', 'external_id', { 
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
