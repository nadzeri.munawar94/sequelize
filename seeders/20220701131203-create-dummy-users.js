'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

      await queryInterface.bulkInsert('Users', [{
        // id: 3,
        username: 'John Doe',
         email: 'john.doe@gmail.com',
         createdAt: '2022-06-29T14:37:15.304Z',
         updatedAt: '2022-06-29T14:37:15.304Z',
      },
      {
        // id: 3,
        username: 'John Doer',
         email: 'john.doer@gmail.com',
         createdAt: '2022-06-29T14:37:15.304Z',
         updatedAt: '2022-06-29T14:37:15.304Z',
      },
      {
        // id: 3,
        username: 'John Doerr',
         email: 'john.doerr@gmail.com',
         createdAt: '2022-06-29T14:37:15.304Z',
         updatedAt: '2022-06-29T14:37:15.304Z',
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     const Op = Sequelize.Op;
     queryInterface.bulkDelete('Users', { email: {[Op.in]: [
      'john.doe@gmail.com',
      'john.doer@gmail.com',
      'john.doerr@gmail.com'
     ]} }, {})
  }
};
