const express = require('express');
const app = express();

const { sequelize, Article, User } = require('./models')

app.set('view engine', 'ejs');
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

// GET all articles
app.get('/articles/create', async (req, res) => {
  const users = await User.findAll();
  res.render('articles/create', { users });
});

// Form for create new user and article
app.get('/create', async (req, res) => {
  res.render('create');
});

// API for handling new user and article
app.post('/create', async (req, res) => {
  const { username, email, title, body } = req.body;

  const t = await sequelize.transaction();
  try {
    const user = await User.create({ username, email }, { transaction: t });
    const article = await Article.create({ title, body, user_id: user.id }, { transaction: t });

    await t.commit();

    res.status(200).json(article);
  } catch(err) {
    await t.rollback();

    res.status(500).json(err);
  }
});

// GET all articles
app.get('/users', (req, res) => {
  User.findAll({ include: Article })
    .then(users => {
      res.status(200).json(users)
    })
 });

 // GET all articles
app.get('/users/:id', (req, res) => {
  User.findOne({
    where: { id: req.params.id },
    include: Article
  })
    .then(user => {
      res.status(200).json(user)
    })
 });

// GET all articles
app.get('/articles', async (req, res) => {
  const articles = await Article.findAll({ include: User });
  res.render('articles/index', { articles });
});

// GET article by ID
app.get('/articles/:id', async (req, res) => {
  const article = await Article.findOne({
    raw: true,
    where: { id: req.params.id },
    include: User
  });

  res.render('articles/show', { ...article })
});

// POST all articles
app.post('/articles', async (req, res) => {
  const { title, body, approved, user_id } = req.body;

  try {
    const createdArticle = await Article.create({ title, body, approved, user_id });
    res.status(200).json(createdArticle);
  } catch(err) {
    res.status(500).json(err);
  }
});

  // PUT article by ID
app.put('/articles/:id', (req, res) => {
    const { title, body, approved } = req.body;
    Article.update({ title, body, approved },
    {
      where: { id: req.params.id }
    })
      .then(article => {
        res.status(200).json(article)
      })
   });

app.listen(8080, 'localhost', () => {
    console.log('Server started');
})
